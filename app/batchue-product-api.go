package app

import (
	"github.com/gin-gonic/gin"
	"log"
)

func InitiServer() {
	log.Println("Starting on http://localhost:9090")
	ginMode := "release"
	gin.SetMode(ginMode)
	serverInstance := gin.New()
	serverInstance.Use(gin.Recovery())

	errorServerInstance := serverInstance.Run(":9090")
	if errorServerInstance != nil {
		panic(errorServerInstance.Error())
	}
}
